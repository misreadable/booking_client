import http from '@/services/http';

export default class Authentication {
  static isAuthenticated () {
    const authToken = window.localStorage.getItem('auth_token')
    return authToken !== null && authToken !== 'undefined'
  }

  static login (phoneNumber) {
    return http.post('/login', {
      phone_number: phoneNumber
    }).then((user) => {
      console.log('user')
    }).catch((err) => {
      console.error('Error happened while login process', err)
    });
  }

  static signup ({ email, firstName, phoneNumber, churchUnion }) {
  }

  static logout () {
    console.log('user asked about logout')
    window.localStorage.removeItem('auth_token')
  }
}
