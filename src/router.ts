import Vue from 'vue'
import Router from 'vue-router'
import HomePage from './views/Home.vue'
import SignupPage from './views/Signup.vue'
import LoginPage from './views/Login.vue'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage
    },
    {
      path: '/signup',
      name: 'signup',
      component: SignupPage
    },
    {
      path: '/login',
      name: 'login',
      component: LoginPage
    }
  ]
});

