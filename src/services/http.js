import axios from 'axios'

export default class http {
  static getConfig () {
    return {
      baseUrl: 'http://localhost:3010/api',
      headers: {
        ...this.headers,
        'content-type': 'application/json'
      }
    };
  }

  static get (uri) {
    const config = this.getConfig();
    return axios.request({
      url: uri,
      method: 'get',
      baseURL: config.baseUrl,
      headers: config.headers,
      responseType: 'json'
    }).then((response) => {
      return response.data;
    });
  }

  static post (uri, data) {
    const config = this.getConfig();
    let requestBody = data;
    if (requestBody === undefined) {
      requestBody = null;
    }
    return axios.request({
      url: uri,
      method: 'post',
      baseURL: config.baseUrl,
      headers: config.headers,
      data: requestBody,
      responseType: 'json'
    }).then((response) => {
      return response.data;
    });
  }

  static update (uri, data) {
    const config = this.getConfig();
    let requestBody = data;
    if (requestBody === undefined) {
      requestBody = null;
    }
    return axios.request({
      url: uri,
      method: 'put',
      baseURL: config.baseUrl,
      headers: config.headers,
      data: requestBody,
      responseType: 'json'
    }).then((response) => {
      return response.data;
    });
  }

  static delete (uri, data) {
    const config = this.getConfig();
    let requestBody = data;
    if (requestBody === undefined) {
      requestBody = null;
    }
    return axios.request({
      url: uri,
      method: 'delete',
      baseURL: config.baseUrl,
      headers: config.headers,
      data: requestBody,
      responseType: 'json'
    }).then((response) => {
      return response.data;
    });
  }
}
